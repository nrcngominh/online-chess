const { createClient } = require("redis");
const { promisify } = require("util");

const client = createClient({
  host: process.env.REDIS_HOST,
  port: process.env.REDIS_PORT,
});

console.log("Connected to Redis");

const get = promisify(client.get).bind(client);
const setex = promisify(client.setex).bind(client);

module.exports = {
  get,
  setex,
};
