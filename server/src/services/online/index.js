const { createGameUrl } = require("./url-generator");

module.exports = {
  createGameUrl,
};
