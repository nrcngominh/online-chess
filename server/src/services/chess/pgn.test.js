const PGN = require("./pgn");
const { createInitPosition } = require("./game");
const { parseSquare } = require("./utils");
const assert = require("assert");

describe("PGN stringify", () => {
  it("should return correct PGN", () => {
    const position = createInitPosition();
    const src = parseSquare("g1");
    const dst = parseSquare("f3");
    const move = PGN.stringify(position, src, dst);
    assert.strictEqual(move, "Nf3");
  });
});
