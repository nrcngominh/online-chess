const parseBoard = (data) => {
  return data
    .split("/")
    .reverse()
    .map((row) =>
      [...row].reduce((acc, ele) => {
        const isEmptySquares = /^\d$/.test(ele);
        return isEmptySquares
          ? [...acc, ...Array(parseInt(ele)).fill(null)]
          : [...acc, ele];
      }, [])
    );
};

// Parse position from FEN
const parse = (fen) => {
  const fields = fen.split(" ");

  const board = parseBoard(fields[0]);
  const whiteToPlay = fields[1] === "w";
  const castling = {
    white: {
      kingside: fields[2].search("K") !== -1,
      queenside: fields[2].search("Q") !== -1,
    },
    black: {
      kingside: fields[2].search("k") !== -1,
      queenside: fields[2].search("q") !== -1,
    },
  };
  const enPassant = fields[3] !== "-" ? fields[3] : null;
  const halfmove = parseInt(fields[4]);
  const fullmove = parseInt(fields[5]);

  return {
    board,
    whiteToPlay,
    castling,
    enPassant,
    halfmove,
    fullmove,
  };
};

// Convert position to FEN
const stringify = (position) => {
  // TODO
};

module.exports = {
  parse,
  stringify,
};
