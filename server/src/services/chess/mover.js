const { parseSquare } = require("./utils");

const isAttackedbyRowOrColPiece = (position, piece) => {
  return (
    (piece === "q" && position.whiteToPlay) ||
    (piece === "r" && position.whiteToPlay) ||
    (piece === "Q" && !position.whiteToPlay) ||
    (piece === "R" && !position.whiteToPlay)
  );
};

const isAttackedbyDiagonalPiece = (position, piece) => {
  return (
    (piece === "q" && position.whiteToPlay) ||
    (piece === "b" && position.whiteToPlay) ||
    (piece === "Q" && !position.whiteToPlay) ||
    (piece === "B" && !position.whiteToPlay)
  );
};

const isAttackedbyKnightPiece = (position, piece) => {
  return (
    (piece === "n" && position.whiteToPlay) ||
    (piece === "N" && !position.whiteToPlay)
  );
};

const isAttackedbyPawnPiece = (position, piece) => {
  return (
    (piece === "p" && position.whiteToPlay) ||
    (piece === "P" && !position.whiteToPlay)
  );
};
// Move
const makeKingsideCastle = (position, src, dst, piece) => {
  // TODO
  position.board[dst.row][dst.col + 1] = null;
  if (piece == "K") {
    position.board[dst.row][dst.col - 1] = "R";
  } else {
    position.board[dst.row][dst.col - 1] = "r";
  }
};

const makeQueensideCastle = (position, src, dst, piece) => {
  // TODO
  position.board[dst.row][dst.col - 2] = null;
  if (piece == "K") {
    position.board[dst.row][dst.col + 1] = "R";
  } else {
    position.board[dst.row][dst.col + 1] = "r";
  }
};

const makeEnPassant = (position, piece, enPassantSquare) => {
  if (piece === "P") {
    position.board[enPassantSquare.row - 1][enPassantSquare.col] = null;
  } else if (piece === "p") {
    position.board[enPassantSquare.row + 1][enPassantSquare.col] = null;
  }
};

const makePromotion = (position, dst, promotion) => {
  position.board[dst.row][dst.col] = promotion;
};

const makeMove = (position, src, dst, promotion) => {
  // TODO
  const enPassantSquare = position.enPassant
    ? parseSquare(position.enPassant)
    : null;
  const piece = position.board[src.row][src.col];
  position.board[src.row][src.col] = null;
  position.board[dst.row][dst.col] = piece;
  if (promotion) {
    makePromotion(position, dst, promotion);
  } else if (piece.toUpperCase() === "K" && dst.col - src.col === 2) {
    makeKingsideCastle(position, src, dst, piece);
  } else if (piece.toUpperCase() === "K" && dst.col - src.col === -2) {
    makeQueensideCastle(position, src, dst, piece);
  } else if (
    enPassantSquare &&
    enPassantSquare.col === dst.col &&
    enPassantSquare.row === dst.row
  ) {
    makeEnPassant(position, piece, enPassantSquare);
  }
};

module.exports = {
  makeMove,
  isAttackedbyRowOrColPiece,
  isAttackedbyDiagonalPiece,
  isAttackedbyKnightPiece,
  isAttackedbyPawnPiece,
};
