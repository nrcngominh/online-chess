const FEN = require("./fen");
const assert = require("assert");

describe("FEN parse", () => {
  describe("Test board", () => {
    it("should return correct board", () => {
      const fen = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";
      const { board } = FEN.parse(fen);

      assert.strictEqual(board[3][4], null);
      assert.deepStrictEqual(board[0][2], "B");
      assert.deepStrictEqual(board[1][3], "P");
      assert.deepStrictEqual(board[6][6], "p");
      assert.deepStrictEqual(board[7][4], "k");
    });
  });

  describe("Test active color", () => {
    it("should return white to play", () => {
      const fen = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";
      const { whiteToPlay } = FEN.parse(fen);
      assert.ok(whiteToPlay);
    });

    it("should return black to play", () => {
      const fen = "rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR b KQkq e3 0 1";
      const { whiteToPlay } = FEN.parse(fen);
      assert.ok(!whiteToPlay);
    });
  });

  describe("Test en passant", () => {
    it("should return null", () => {
      const fen = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";
      const { enPassant } = FEN.parse(fen);
      assert.strictEqual(enPassant, null);
    });

    it("should return valid square", () => {
      const fen =
        "rnbqkbnr/pp1p1ppp/8/2pPp3/8/8/PPP1PPPP/RNBQKBNR w KQkq e6 0 3";
      const { enPassant } = FEN.parse(fen);
      assert.strictEqual(enPassant, "e6");
    });
  });

  describe("Test castling", () => {
    it("should return cannot castling", () => {
      const fen = "rnbq1bnr/ppppkppp/4p3/8/8/4P3/PPPPKPPP/RNBQ1BNR w - - 2 3";
      const {
        castling: { white, black },
      } = FEN.parse(fen);
      assert.strictEqual(white.kingside, false);
      assert.strictEqual(white.queenside, false);
      assert.strictEqual(black.kingside, false);
      assert.strictEqual(black.queenside, false);
    });

    it("should return correct castling availability", () => {
      const fen =
        "r1bqkb1r/pppp1ppp/2n2n2/1B2p3/4P3/5N2/PPPP1PPP/RNBQ1RK1 b kq - 5 4";
      const {
        castling: { white, black },
      } = FEN.parse(fen);
      assert.strictEqual(white.kingside, false);
      assert.strictEqual(white.queenside, false);
      assert.strictEqual(black.kingside, true);
      assert.strictEqual(black.queenside, true);
    });
  });

  describe("Test halfmove", () => {
    it("should return correct halfmove", () => {
      const fen =
        "r1bqkb1r/pppp1ppp/2n2n2/1B2p3/4P3/5N2/PPPP1PPP/RNBQ1RK1 b kq - 5 4";
      const { halfmove } = FEN.parse(fen);
      assert.strictEqual(halfmove, 5);
    });
  });

  describe("Test fullmove", () => {
    it("should return correct fullmove", () => {
      const fen =
        "r1bqkb1r/pppp1ppp/2n2n2/1B2p3/4P3/5N2/PPPP1PPP/RNBQ1RK1 b kq - 5 4";
      const { fullmove } = FEN.parse(fen);
      assert.strictEqual(fullmove, 4);
    });
  });
});

describe("FEN stringify", () => {
  it("should return correct FEN", () => {
    const position = {
      board: [
        [..."rnbqkbnr"],
        [..."pppppppp"],
        [null, null, null, null, null, null, null, null],
        [null, null, null, null, null, null, null, null],
        [null, null, null, null, null, null, null, null],
        [null, null, null, null, null, null, null, null],
        [null, null, null, null, null, null, null, null],
        [null, null, null, null, null, null, null, null],
        [..."PPPPPPPP"],
        [..."RNBQKBNR"],
      ].reverse(),
      whiteToPlay: true,
      castling: {
        white: {
          kingside: true,
          queenside: true,
        },
        black: {
          kingside: true,
          queenside: true,
        },
      },
      enPassant: null,
      halfmove: 0,
      fullmove: 1,
    };

    const fen = FEN.stringify(position);
    const expected = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";
    assert.deepStrictEqual(fen, expected);
  });
});
