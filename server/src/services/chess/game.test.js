const FEN = require("./fen.js");
const { createInitPosition, canMove, isCheckmate } = require("./game");
const { parseSquare } = require("./utils");
const assert = require("assert");

describe("Piece rule", () => {
  describe("Knight rule", () => {
    it("should return true for moving to empty square", () => {
      const position = createInitPosition();
      assert.ok(canMove(position, parseSquare("g1"), parseSquare("f3")));
    });

    it("should return false for moving to my team", () => {
      const position = FEN.parse(
        "rnbqkbnr/pppp1ppp/8/4p3/8/5N2/PPPPPPPP/RNBQKB1R w KQkq - 0 2"
      );
      assert.ok(!canMove(position, parseSquare("f3"), parseSquare("e1")));
    });

    it("should return true for capturing", () => {
      const position = FEN.parse(
        "rnbqkbnr/pppp1ppp/8/4p3/8/5N2/PPPPPPPP/RNBQKB1R w KQkq - 0 2"
      );
      assert.ok(canMove(position, parseSquare("f3"), parseSquare("e5")));
    });

    it("should return false for invalid square", () => {
      const position = FEN.parse(
        "rnbqkbnr/pppp1ppp/8/4p3/8/5N2/PPPPPPPP/RNBQKB1R w KQkq - 0 2"
      );
      assert.ok(!canMove(position, parseSquare("f3"), parseSquare("d3")));
    });
  });

  describe("Rook rule", () => {
    it("should return false if be blocked by col", () => {
      const position = createInitPosition();
      assert.ok(!canMove(position, parseSquare("h1"), parseSquare("h5")));
    });
    it("should return false if be blocked by row", () => {
      const position = createInitPosition();
      assert.ok(!canMove(position, parseSquare("h1"), parseSquare("e1")));
    });
    it("should return false if move wrong rule ", () => {
      const position = createInitPosition();
      assert.ok(!canMove(position, parseSquare("h1"), parseSquare("h3")));
    });
    it("should return false if dst is crew", () => {
      const position = FEN.parse(
        "rnbqkbnr/pp1p1ppp/8/2p1p3/7P/5N2/PPPPPPP1/RNBQKB1R w KQkq - 0 3"
      );
      assert.ok(!canMove(position, parseSquare("h1"), parseSquare("h4")));
    });
    it("should return true if dst is enemy", () => {
      const position = FEN.parse(
        "rnbqkbnr/pp3p1p/3p4/2p1N3/7p/8/PPPPPPP1/RNBQKB1R w Qkq - 0 6"
      );
      assert.ok(canMove(position, parseSquare("h1"), parseSquare("h4")));
    });
    it("should return true if move right rule", () => {
      const position = FEN.parse(
        "rnbqkbnr/pp1p1ppp/8/2p1p3/7P/5N2/PPPPPPP1/RNBQKB1R w KQkq - 0 3"
      );
      assert.ok(canMove(position, parseSquare("h1"), parseSquare("h3")));
    });
  });

  describe("Bishop rule", () => {
    it("should return false if be blocked by cross", () => {
      const position = createInitPosition();
      assert.ok(!canMove(position, parseSquare("f1"), parseSquare("d3")));
    });

    it("should return true if move right rule", () => {
      const position = FEN.parse(
        "rnbqkbnr/ppppp1pp/8/5p2/4P3/8/PPPP1PPP/RNBQKBNR w KQkq - 0 2"
      );
      assert.ok(canMove(position, parseSquare("f1"), parseSquare("d3")));
    });

    it("should return true if eat enemy", () => {
      const position = FEN.parse(
        "rnbqkbnr/ppp3pp/8/3p4/6p1/2P5/PP1P1PBP/RNBQK1NR w KQkq - 0 6"
      );
      assert.ok(canMove(position, parseSquare("g2"), parseSquare("d5")));
    });

    it("should return false if move wrong rule", () => {
      const position = FEN.parse(
        "rnbqkbnr/ppp3pp/8/3p4/6p1/2P5/PP1P1PBP/RNBQK1NR w KQkq - 0 6"
      );
      assert.ok(!canMove(position, parseSquare("g2"), parseSquare("d3")));
    });

    it("should return false if be blocked", () => {
      const position = FEN.parse(
        "rnbqkbnr/ppp3pp/8/3p4/6p1/2P5/PP1P1PBP/RNBQK1NR w KQkq - 0 6"
      );
      assert.ok(!canMove(position, parseSquare("g2"), parseSquare("c6")));
    });
  });

  describe("Queen rule", () => {
    it("should return false if be blocked by cross", () => {
      const position = createInitPosition();
      assert.ok(!canMove(position, parseSquare("d1"), parseSquare("d3")));
    });

    it("should return true if move right rule", () => {
      const position = FEN.parse(
        "rnbqkbnr/ppp3pp/8/3p4/6p1/2P5/PP1P1PBP/RNBQK1NR w KQkq - 0 6"
      );
      assert.ok(canMove(position, parseSquare("d1"), parseSquare("b3")));
    });

    it("should return true if eat enemy", () => {
      const position = FEN.parse(
        "rnbqkbnr/p1p3pp/8/3P4/6p1/1p6/PPQP1PBP/RNB1K1NR w KQkq - 0 9"
      );
      assert.ok(canMove(position, parseSquare("c2"), parseSquare("b3")));
    });

    it("should return false if move wrong rule", () => {
      const position = FEN.parse(
        "rnbqkbnr/p1p3pp/8/3P4/6p1/1p6/PPQP1PBP/RNB1K1NR w KQkq - 0 9"
      );
      assert.ok(!canMove(position, parseSquare("c2"), parseSquare("d4")));
    });

    it("should return false cause king is attacked in line", () => {
      const position = FEN.parse(
        "rnb1kbnr/pp4pp/4p3/2p1q3/2P3P1/8/PP1PQP1P/RNB1KBNR w KQkq - 1 7"
      );
      assert.ok(!canMove(position, parseSquare("e2"), parseSquare("d3")));
    });
  });

  describe("Pawn rule", () => {
    it("should return true if white first move 2", () => {
      const position = createInitPosition();
      assert.ok(canMove(position, parseSquare("f2"), parseSquare("f4")));
    });

    it("should return true if black first move 2", () => {
      const position = FEN.parse(
        "rnbqkbnr/pppppppp/8/8/5P2/8/PPPPP1PP/RNBQKBNR b KQkq - 0 1"
      );
      assert.ok(canMove(position, parseSquare("c7"), parseSquare("c5")));
    });

    it("should return true if white first move 1", () => {
      const position = createInitPosition();
      assert.ok(canMove(position, parseSquare("f2"), parseSquare("f3")));
    });

    it("should return true if black first move 1", () => {
      const position = FEN.parse(
        "rnbqkbnr/pppppppp/8/8/8/5P2/PPPPP1PP/RNBQKBNR b KQkq - 0 1"
      );
      assert.ok(canMove(position, parseSquare("c7"), parseSquare("c6")));
    });
  });

  describe("King rule", () => {
    it("should return false if be blocked", () => {
      const position = createInitPosition();
      assert.ok(!canMove(position, parseSquare("e1"), parseSquare("e2")));
    });

    it("should return true if king move in line", () => {
      const position = FEN.parse(
        "rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR w KQkq - 0 2"
      );
      assert.ok(canMove(position, parseSquare("e1"), parseSquare("e2")));
    });

    it("should return true if king move in diagonal", () => {
      const position = FEN.parse(
        "rnbqkbnr/p2ppppp/8/1pp5/3PP3/8/PPP2PPP/RNBQKBNR w KQkq - 0 3"
      );
      assert.ok(canMove(position, parseSquare("e1"), parseSquare("d2")));
    });

    it("should return false if king be attacked by line", () => {
      const position = FEN.parse(
        "r1b1kbnr/p2ppppp/2n5/4q3/2Pp4/8/PPQ2PPP/RNB2KNR w kq - 2 9"
      );
      assert.ok(!canMove(position, parseSquare("f1"), parseSquare("e1")));
    });
  });

  describe("King is attacked", () => {
    it("should return false if king be attacked by diagonal", () => {
      const position = FEN.parse(
        "rnb1kbnr/pp1ppppp/8/q1p5/3P4/8/PPPQPPPP/RNB1KBNR w KQkq - 2 3"
      );
      assert.ok(!canMove(position, parseSquare("d2"), parseSquare("d3")));
    });

    it("should return true if king not be attacked", () => {
      const position = FEN.parse(
        "r4bnr/2B1k1pp/4B2N/3p1P2/p2P4/3b2n1/pq5Q/R4KR1 w - - 1 30"
      );
      assert.ok(canMove(position, parseSquare("f1"), parseSquare("e1")));
    });

    it("should return false if king be attacked by diagonal", () => {
      const position = FEN.parse(
        "rnb1kbnr/pppp1ppp/8/8/3p1BP1/2P5/P3BP2/Rq1QK1NR w KQkq - 0 9"
      );
      assert.ok(!canMove(position, parseSquare("d1"), parseSquare("a1")));
    });

    it("should return false if king be attacked by knight", () => {
      const position = FEN.parse(
        "r1b1kbnr/pp1ppppp/8/Q2P4/2p5/4Pn2/PPP2PPP/RNBK1BNR w kq - 2 7"
      );
      assert.ok(!canMove(position, parseSquare("d1"), parseSquare("e1")));
    });

    it("should return false if white king be attacked by black pawn", () => {
      const position = FEN.parse(
        "r1b1kbnr/pp1p1ppp/8/Q2Pp3/4P3/2p2P2/PPP2P1P/RNBK1BNR w kq - 0 9"
      );
      assert.ok(!canMove(position, parseSquare("d1"), parseSquare("d2")));
    });

    it("should return false if black king be attacked by white pawn", () => {
      const position = FEN.parse(
        "rnbqkbnr/pp1pp1pp/4Pp2/8/2p4P/8/PPPP1PP1/RNBQKBNR b KQkq - 0 4"
      );
      assert.ok(!canMove(position, parseSquare("e8"), parseSquare("f7")));
    });
  });

  describe("Pawn Enpassant", () => {
    it("should return true when Pawn enpassant", () => {
      const position = FEN.parse(
        "rnbqkNQr/p4p1p/2p5/1p1B4/2Pp4/4bp2/PP1n1R1P/RNBQ2K1 b kq c3 0 17"
      );
      assert.ok(canMove(position, parseSquare("d4"), parseSquare("c3")));
    });
    it("should return false when Pawn want enpassant", () => {
      const position = FEN.parse(
        "rnbqkNQr/p4p1p/2p5/8/1pPpB3/4bp2/PP1n1R1P/RNBQ2K1 b kq - 1 18"
      );
      assert.ok(!canMove(position, parseSquare("d4"), parseSquare("c3")));
    });
  });

  describe("Castle", () => {
    it("should return true when Queen Black Castle", () => {
      const position = FEN.parse(
        "r3kN1r/p4p1p/2p5/1Q4q1/1p1pB3/3nbp1b/PP1n1R1P/RNBQ3K b kq - 3 23"
      );
      assert.ok(canMove(position, parseSquare("e8"), parseSquare("c8")));
    });

    it("should return true when King Black Castle", () => {
      const position = FEN.parse(
        "r3k2r/p6p/2p5/1QN2pq1/1p1pB3/3nbp1b/PP1n1R1P/RNBQ3K b kq - 1 25"
      );
      assert.ok(canMove(position, parseSquare("e8"), parseSquare("g8")));
    });

    it("should return true when Queen White Castle", () => {
      const position = FEN.parse(
        "rnbqkbnr/pp1p2pp/5p2/2p5/2PPpB2/2NQ4/PP2PPPP/R3KBNR w KQkq - 0 6"
      );
      assert.ok(canMove(position, parseSquare("e1"), parseSquare("c1")));
    });

    it("should return true when King White Castle", () => {
      const position = FEN.parse(
        "rnbqkbnr/pp4pp/5p2/2pp4/2PP1B2/2NBPN2/PP3PPP/R3K2R w KQkq - 0 9"
      );
      assert.ok(canMove(position, parseSquare("e1"), parseSquare("g1")));
    });

    it("should return false when Rook Queen move", () => {
      const position = FEN.parse(
        "rnbqkbnr/p5pp/1p6/2p2p2/2pP1B2/2N1PN2/PP2BPPP/1R2K1R1 w kq - 0 12"
      );
      assert.ok(!canMove(position, parseSquare("e1"), parseSquare("c1")));
    });

    it("should return false when Rook King move", () => {
      const position = FEN.parse(
        "rnbqkbnr/pp4pp/8/2pp1p2/2PP1B2/2NBPN2/PP3PPP/R3K1R1 w Qkq - 0 10"
      );
      assert.ok(!canMove(position, parseSquare("e1"), parseSquare("g1")));
    });
  });

  describe("isCheckMate", () => {
    it("should return false when not checkmate", () => {
      const position = FEN.parse(
        "r4bnr/2B1k1pp/4B2N/3p1P2/p2P4/3b2n1/pq5Q/R4KR1 w - - 1 30"
      );
      assert.ok(!isCheckmate(position));
    });

    it("should return true when checkmate", () => {
      const position = FEN.parse(
        "rnb1k1nr/pp1p1pPp/1q2p3/5P2/2p5/8/PPPPPbP1/RNBQKBNR w KQkq - 1 7"
      );
      assert.ok(isCheckmate(position));
    });

    it("should return true when checkmate", () => {
      const position = FEN.parse(
        "rnbqkbnr/2ppp1pp/8/1p2Pp2/p3K3/1P6/P1PP1PPP/RNBQ1BNR w kq f6 0 7"
      );
      assert.ok(canMove(position, parseSquare("e5"), parseSquare("f6")));
    });
  });
});
