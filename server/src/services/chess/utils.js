const parseSquare = (square) => {
  const col = square.charCodeAt(0) - "a".charCodeAt(0);
  const row = parseInt(square[1] - 1);
  return { row, col };
};

module.exports = {
  parseSquare,
};
