const FEN = require("./fen.js");
const { makeMove } = require("./mover");
const { parseSquare } = require("./utils");
const assert = require("assert");
const { createInitPosition, canMove } = require("./game");

describe("Piece rule", () => {
  describe("Enpassant move", () => {
    it("Pawn move and capture", () => {
      const position = FEN.parse(
        "rnbqkbnr/ppp1p1pp/8/3pPp2/3P4/8/PPP2PPP/RNBQKBNR w KQkq f6 0 4"
      );
      const src = parseSquare("e5");
      const dst = parseSquare("f6");
      makeMove(position, src, dst, null);
      assert.strictEqual(position.board[src.row][src.col], null);
      assert.strictEqual(position.board[dst.row][dst.col], "P");
      assert.strictEqual(position.board[dst.row - 1][dst.col], null);
    });

    it("Pawn just move, don;t enpassant", () => {
      const position = FEN.parse(
        "rnbqkbnr/pp2p1pp/2p5/3pPp2/3P3P/8/PPP2PP1/RNBQKBNR w KQkq - 0 5"
      );
      const src = parseSquare("e5");
      const dst = parseSquare("f6");
      makeMove(position, src, dst, null);
      assert.strictEqual(position.board[src.row][src.col], null);
      assert.strictEqual(position.board[dst.row][dst.col], "P");
      assert.strictEqual(position.board[dst.row - 1][dst.col], "p");
    });

    it("Bishop move, not capture", () => {
      const position = FEN.parse(
        "rnbqkbnr/p1p1p1pp/8/1p1pPpB1/3P4/8/PPP2PPP/RN1QKBNR w KQkq f6 0 5"
      );
      const src = parseSquare("g5");
      const dst = parseSquare("f6");
      makeMove(position, src, dst, null);
      assert.strictEqual(position.board[src.row][src.col], null);
      assert.strictEqual(position.board[dst.row][dst.col], "B");
      assert.strictEqual(position.board[dst.row - 1][dst.col], "p");
    });
  });

  describe("Castle move", () => {
    it("queen castle white", () => {
      const position = FEN.parse(
        "rnbqkbnr/p4ppp/1pp1p3/3p2B1/3P4/2NQ4/PPP1PPPP/R3KBNR w KQkq - 0 5"
      );
      const src = parseSquare("e1");
      const dst = parseSquare("c1");
      makeMove(position, src, dst, null);
      assert.strictEqual(position.board[src.row][src.col], null);
      assert.strictEqual(position.board[dst.row][dst.col - 2], null);
      assert.strictEqual(position.board[dst.row][dst.col + 1], "R");
      assert.strictEqual(position.board[dst.row][dst.col], "K");
    });

    it("queen castle black", () => {
      const position = FEN.parse(
        "r3kbnr/p1qb2pp/npp1pp2/3p4/3P1B2/2NQP3/PPP2PPP/2KR1BNR b kq - 1 9"
      );
      const src = parseSquare("e8");
      const dst = parseSquare("c8");
      makeMove(position, src, dst, null);
      assert.strictEqual(position.board[src.row][src.col], null);
      assert.strictEqual(position.board[dst.row][dst.col - 2], null);
      assert.strictEqual(position.board[dst.row][dst.col + 1], "r");
      assert.strictEqual(position.board[dst.row][dst.col], "k");
    });

    it("king castle white", () => {
      const position = FEN.parse(
        "rnbqkbnr/ppp2ppp/3p4/4p3/2B5/4PN2/PPPP1PPP/RNBQK2R w KQkq - 0 4"
      );
      const src = parseSquare("e1");
      const dst = parseSquare("g1");
      makeMove(position, src, dst, null);
      assert.strictEqual(position.board[src.row][src.col], null);
      assert.strictEqual(position.board[dst.row][dst.col], "K");
      assert.strictEqual(position.board[dst.row][dst.col + 1], null);
      assert.strictEqual(position.board[dst.row][dst.col - 1], "R");
    });

    it("king castle black", () => {
      const position = FEN.parse(
        "rnbqk2r/ppp2p1p/3p1npb/4p3/2B1P2N/5P2/PPPP2PP/RNBQ1RK1 b kq - 2 7"
      );
      const src = parseSquare("e8");
      const dst = parseSquare("g8");
      makeMove(position, src, dst, null);
      assert.strictEqual(position.board[src.row][src.col], null);
      assert.strictEqual(position.board[dst.row][dst.col], "k");
      assert.strictEqual(position.board[dst.row][dst.col + 1], null);
      assert.strictEqual(position.board[dst.row][dst.col - 1], "r");
    });
  });

  describe("Promotion move", () => {
    it("Pawn move and promotion", () => {
      const position = FEN.parse(
        "rnbqkN1r/p4pPp/1pp5/3p4/2B5/4bp2/PPPn1R1P/RNBQ2K1 w kq - 0 15"
      );
      const src = parseSquare("g7");
      const dst = parseSquare("g8");
      makeMove(position, src, dst, "Q");
      assert.strictEqual(position.board[src.row][src.col], null);
      assert.strictEqual(position.board[dst.row][dst.col], "Q");
    });
  });
});
