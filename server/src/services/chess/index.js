const { canMove, isCheckmate, createInitPosition } = require("./game");
const { makeMove } = require("./mover");
const { parseSquare } = require("./utils");
const FEN = require("./fen");
const PGN = require("./pgn");

module.exports = {
  canMove,
  makeMove,
  isCheckmate,
  createInitPosition,
  parseSquare,
  FEN,
  PGN,
};
