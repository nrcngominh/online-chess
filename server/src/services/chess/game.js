const FEN = require("./fen");
const mover = require("./mover.js");
const { parseSquare } = require("./utils");

const createInitPosition = () => {
  const initFen = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";
  return FEN.parse(initFen);
};

// Utils
const isValid = (row, col) => {
  return row >= 0 && row <= 7 && col >= 0 && col <= 7;
};

const isWhitePiece = (piece) => /^[A-Z]$/.test(piece);
const isEmptyOrEnemy = (position, dst) => {
  const piece = position.board[dst.row][dst.col];
  return piece === null || position.whiteToPlay !== isWhitePiece(piece);
};
const isEnemy = (position, dst) => {
  const piece = position.board[dst.row][dst.col];
  return piece !== null && position.whiteToPlay !== isWhitePiece(piece);
};

const isSameRow = (src, dst) => src.row === dst.row;
const isSameCol = (src, dst) => src.col === dst.col;
const isSameDiagonal = (src, dst) =>
  Math.abs(src.row - dst.row) === Math.abs(src.col - dst.col);
const isAdjacency = (src, dst) =>
  Math.abs(dst.row - src.row) <= 1 && Math.abs(dst.col - src.col) <= 1;

const nonBlockRow = (board, src, dst) => {
  return src.col > dst.col
    ? nonBlockRow(board, dst, src)
    : [...Array(dst.col - src.col - 1).keys()]
        .map((iCol) => iCol + src.col + 1)
        .every((iCol) => board[src.row][iCol] === null);
};

const nonBlockCol = (board, src, dst) => {
  return src.row > dst.row
    ? nonBlockCol(board, dst, src)
    : [...Array(dst.row - src.row - 1).keys()]
        .map((iRow) => iRow + src.row + 1)
        .every((iRow) => board[iRow][src.col] === null);
};

const nonBlockDiagonal = (board, src, dst) => {
  const distance = Math.abs(src.row - dst.row);
  const rowFactor = dst.row - src.row > 0 ? 1 : -1;
  const colFactor = dst.col - src.col > 0 ? 1 : -1;
  return [...Array(distance - 1).keys()]
    .map((i) => i + 1)
    .every((i) => {
      const row = src.row + rowFactor * i;
      const col = src.col + colFactor * i;
      return board[row][col] === null;
    });
};

// Check if desination is attacked by enemy rook, queen
const isAttackedSameRow = (position, dst) => {
  for (let i = dst.col - 1; i >= 0; i--) {
    if (position.board[dst.row][i] !== null) {
      if (mover.isAttackedbyRowOrColPiece(position, position.board[dst.row][i]))
        return true;
      else break;
    }
  }
  for (let i = dst.col + 1; i <= 7; i++) {
    if (position.board[dst.row][i] !== null) {
      if (mover.isAttackedbyRowOrColPiece(position, position.board[dst.row][i]))
        return true;
      else break;
    }
  }
  return false;
};

// Check if desination is attacked by enemy rook, queen
const isAttackedSameCol = (position, dst) => {
  for (let i = dst.row - 1; i >= 0; i--) {
    if (position.board[i][dst.col] !== null) {
      if (mover.isAttackedbyRowOrColPiece(position, position.board[i][dst.col]))
        return true;
      else break;
    }
  }
  for (let i = dst.row + 1; i <= 7; i++) {
    if (position.board[i][dst.col] !== null) {
      if (mover.isAttackedbyRowOrColPiece(position, position.board[i][dst.col]))
        return true;
      else break;
    }
  }
  return false;
};

const runDiagonal = (position, dst, fact) => {
  for (let i = 1; i < 7; i++) {
    const row = dst.row + i * fact[0];
    const col = dst.col + i * fact[1];
    if (isValid(row, col)) {
      if (position.board[row][col] !== null) {
        if (mover.isAttackedbyDiagonalPiece(position, position.board[row][col]))
          return false;
        else break;
      }
    } else {
      break;
    }
  }
  return true;
};

// Check if desination is attacked by enemy queen, bishop
const isAttackedSameDiagonal = (position, dst) => {
  const factor = [
    [-1, -1],
    [-1, 1],
    [1, -1],
    [1, 1],
  ];
  return !factor.every((fact) => runDiagonal(position, dst, fact));
};

// Check if desination is attacked by knight
const isAttackedByKnight = (position, dst) => {
  const knightArray = [
    [1, 2],
    [1, -2],
    [-1, 2],
    [-1, -2],
    [2, 1],
    [2, -1],
    [-2, 1],
    [-2, -1],
  ];
  for (let i = 0; i < 8; i++) {
    const row = dst.row + knightArray[i][0];
    const col = dst.col + knightArray[i][1];
    if (
      isValid(row, col) &&
      position.board[row][col] !== null &&
      mover.isAttackedbyKnightPiece(position, position.board[row][col])
    ) {
      return true;
    }
  }
  return false;
};

const isAttackedByKing = (position, dst) => {
  const { board, whiteToPlay } = position;
  for (let i = -1; i <= 1; i++) {
    for (let j = -1; j <= 1; j++) {
      //make sure not undefined array
      if (isValid(dst.row + i, dst.col + j)) {
        if (
          (board[dst.row + i][dst.col + j] === "K" && !whiteToPlay) ||
          (board[dst.row + i][dst.col + j] === "k" && whiteToPlay)
        ) {
          return true;
        }
      }
    }
  }
  return false;
};

const isAttackedByPawn = (position, dst) => {
  const pawnArray = position.whiteToPlay
    ? [
        [1, -1],
        [1, 1],
      ]
    : [
        [-1, -1],
        [-1, 1],
      ];
  for (let i = 0; i < 2; i++) {
    const row = dst.row + pawnArray[i][0];
    const col = dst.col + pawnArray[i][1];
    if (
      isValid(row, col) &&
      position.board[row][col] !== null &&
      mover.isAttackedbyPawnPiece(position, position.board[row][col])
    ) {
      return true;
    }
  }
  return false;
};

const isAttacked = (position, dst) => {
  return (
    isAttackedByKnight(position, dst) ||
    isAttackedByPawn(position, dst) ||
    isAttackedSameCol(position, dst) ||
    isAttackedSameRow(position, dst) ||
    isAttackedSameDiagonal(position, dst) ||
    isAttackedByKing(position, dst)
  );
};

const findKing = (position) => {
  for (let row = 0; row < 8; row++) {
    for (let col = 0; col < 8; col++) {
      const piece = position.board[row][col];
      if (
        (piece === "K" && position.whiteToPlay) ||
        (piece === "k" && !position.whiteToPlay)
      ) {
        return { row, col };
      }
    }
  }
};

const isKingSafe = (position, src, dst) => {
  const backup = JSON.parse(JSON.stringify(position));
  mover.makeMove(backup, src, dst, null);

  const king = findKing(backup);
  if (!isAttacked(backup, king)) {
    return true;
  } else {
    return false;
  }
};

const isOneAttacked = (position, src, dst) => {
  greater_src = { row: src.row, col: src.col + 1 };
  less_src = { row: src.row, col: src.col - 1 };
  return dst.col > src.col
    ? !isAttacked(position, greater_src)
    : !isAttacked(position, less_src);
};

const isCastle = (position, src, dst) => {
  const validSquare = Math.abs(dst.row - src.row) === 0;
  const kingWhiteCastle =
    dst.col - src.col === 2 &&
    position.castling.white.kingside &&
    position.whiteToPlay;
  const kingBlackCastle =
    dst.col - src.col === 2 &&
    position.castling.black.kingside &&
    !position.whiteToPlay;
  const queenWhiteCastle =
    position.castling.white.queenside &&
    position.whiteToPlay &&
    dst.col - src.col === -2;
  const queenBlackCastle =
    position.castling.black.queenside &&
    !position.whiteToPlay &&
    dst.col - src.col === -2;

  return (
    validSquare &&
    (kingWhiteCastle ||
      kingBlackCastle ||
      queenBlackCastle ||
      queenWhiteCastle) &&
    isOneAttacked(position, src, dst) &&
    nonBlockRow(position.board, src, dst)
  );
};
// Rules
const canKingMove = (position, src, dst) => {
  const castle = isCastle(position, src, dst);
  return (
    (isAdjacency(src, dst) || castle) &&
    isEmptyOrEnemy(position, dst) &&
    !isAttacked(position, dst)
  );
};

const canQueenMove = (position, src, dst) => {
  const board = position.board;
  const validSquare =
    (isSameCol(src, dst) && nonBlockCol(board, src, dst)) ||
    (isSameRow(src, dst) && nonBlockRow(board, src, dst)) ||
    (isSameDiagonal(src, dst) && nonBlockDiagonal(board, src, dst));
  return (
    validSquare &&
    isEmptyOrEnemy(position, dst) &&
    isKingSafe(position, src, dst)
  );
};

const canRookMove = (position, src, dst) => {
  const board = position.board;
  const validSquare =
    (isSameCol(src, dst) && nonBlockCol(board, src, dst)) ||
    (isSameRow(src, dst) && nonBlockRow(board, src, dst));
  return (
    validSquare &&
    isEmptyOrEnemy(position, dst) &&
    isKingSafe(position, src, dst)
  );
};

const canBishopMove = (position, src, dst) => {
  const board = position.board;
  const validSquare =
    isSameDiagonal(src, dst) && nonBlockDiagonal(board, src, dst);
  return (
    validSquare &&
    isEmptyOrEnemy(position, dst) &&
    isKingSafe(position, src, dst)
  );
};

const canKnightMove = (position, src, dst) => {
  const distanceRow = Math.abs(dst.row - src.row);
  const distanceCol = Math.abs(dst.col - src.col);
  const isValidSquare =
    (distanceRow === 1 && distanceCol === 2) ||
    (distanceRow === 2 && distanceCol === 1);
  return (
    isValidSquare &&
    isEmptyOrEnemy(position, dst) &&
    isKingSafe(position, src, dst)
  );
};

const canPawnMove = (position, src, dst) => {
  const { board, whiteToPlay } = position;
  const firstMove = src.row === 1 || src.row === 6;
  const enPassantSquare = position.enPassant
    ? parseSquare(position.enPassant)
    : null;
  const straightMove = isSameCol(src, dst) && nonBlockCol(board, src, dst);
  const oneSquare = whiteToPlay
    ? dst.row - src.row === 1
    : dst.row - src.row === -1;
  const twoSquare =
    firstMove && whiteToPlay
      ? dst.row - src.row === 2
      : dst.row - src.row === -2;
  const capture =
    Math.abs(src.col - dst.col) === 1 && oneSquare && isEnemy(position, dst);
  const enpassant =
    position.enPassant &&
    enPassantSquare.row === dst.row &&
    enPassantSquare.col === dst.col;
  return (
    ((straightMove && (oneSquare || twoSquare)) || capture || enpassant) &&
    isKingSafe(position, src, dst)
  );
};

//checkmate

const moveAroundSquare = (position, src, factor) => {
  for (let i = -factor; i <= factor; i++) {
    for (let j = -factor; j <= factor; j++) {
      if (i === 0 && j === 0) {
        continue;
      }
      const row = src.row + i;
      const col = src.col + j;
      if (isValid(row, col)) {
        if (canMove(position, src, { row, col })) return true;
      }
    }
  }
};

const canMoveForNotCheckMate = (position, piece, src) => {
  if (piece.toUpperCase() === "P" || piece.toUpperCase() === "K") {
    return moveAroundSquare(position, src, 2);
  }
  if (piece.toUpperCase() === "K") {
    return moveAroundSquare(position, src, 1);
  }
  if (
    piece.toUpperCase() === "R" ||
    piece.toUpperCase() === "B" ||
    piece.toUpperCase() === "Q"
  ) {
    return moveAroundSquare(position, src, 7);
  }
  if (piece.toUpperCase() === "N") {
    return moveAroundSquare(position, src, 2);
  }
};

const isCheckmate = (position) => {
  const king = findKing(position);
  if (!isAttacked(position, king)) {
    return false;
  }
  //search full board, pick every piece,
  for (let row = 0; row < 8; row++) {
    for (let col = 0; col < 8; col++) {
      const piece = position.board[row][col];
      if (
        piece !== null &&
        ((isWhitePiece(piece) && position.whiteToPlay) ||
          (!isWhitePiece(piece) && !position.whiteToPlay))
      ) {
        const src = { row, col };
        if (canMoveForNotCheckMate(position, piece, src)) {
          return false;
        }
      }
    }
  }
  return true;
};

const canMove = (position, src, dst) => {
  const rules = {
    K: canKingMove,
    Q: canQueenMove,
    R: canRookMove,
    B: canBishopMove,
    N: canKnightMove,
    P: canPawnMove,
  };
  const pieceCode = position.board[src.row][src.col].toUpperCase();
  return rules[pieceCode](position, src, dst);
};

module.exports = {
  createInitPosition,
  canMove,
  isCheckmate,
};
