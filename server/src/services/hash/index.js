const bcrypt = require("bcryptjs");

const SALT = process.env.SALT || 10;

const hashPassword = async (password) => {
  const salt = await bcrypt.genSalt(SALT);
  return await bcrypt.hash(password, salt);
};

const checkPassword = async (password, hashedPassword) => {
  return await bcrypt.compare(password, hashedPassword);
};

module.exports = {
  hashPassword,
  checkPassword,
};
