const { hashPassword, checkPassword } = require(".");

const assert = require("assert");

describe("Hash", () => {
  it("should return true for comparison", async () => {
    const password = "abcd1234";
    const hashedPassword = await hashPassword(password);
    assert.ok(checkPassword(password, hashedPassword));
  });
});
