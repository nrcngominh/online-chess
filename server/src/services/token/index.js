const jwt = require("jsonwebtoken");

const generateToken = (data, secretKey, tokenLife) => {
  return new Promise((resolve, reject) => {
    try {
      const token = jwt.sign({ user: data }, secretKey, {
        algorithm: "HS256",
        expiresIn: tokenLife,
      });
      resolve(token);
    } catch (error) {
      reject(error);
    }
  });
};

const verifyToken = (token, secretKey) => {
  return new Promise((resolve, reject) => {
    try {
      const decoded = jwt.verify(token, secretKey);
      resolve(decoded);
    } catch (error) {
      reject(error);
    }
  });
};

module.exports = {
  generateToken,
  verifyToken,
};
