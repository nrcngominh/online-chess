const { generateToken, verifyToken } = require("./index");
const assert = require("assert");

describe("Token", () => {
  it("should verify token", async () => {
    const secretKey = "123";
    const newToken = await generateToken("hoang", secretKey, 3600);
    const isValid = await verifyToken(newToken, secretKey);
    assert.ok(isValid);
  });
});
