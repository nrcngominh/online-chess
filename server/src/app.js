const express = require("express");
const cors = require("cors");

module.exports = () => {
  const app = express();

  // Basic middlewares
  app.use(express.json());
  app.use(cors());

  // Router
  app.use("/api", require("./api"));

  return app;
};
