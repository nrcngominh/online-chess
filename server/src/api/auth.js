const { Router } = require("express");
const { verifyToken } = require("../services/token");
const router = Router();
const secretKey = process.env.JWT_SECRET_KEY || "onlinechess";

router.post("/", async (req, res) => {
  const { token } = req.body;
  if (!token) {
    res.status(400).send({
      msg: "Invalid Token",
    });
  } else {
    try {
      const {
        user: { username },
      } = await verifyToken(token, secretKey);
      res.status(200).send({
        msg: "Login successful",
        data: {
          username,
        },
      });
    } catch (error) {
      res.status(401).send({
        msg: "Invalid token",
      });
    }
  }
});

module.exports = router;
