const { checkPassword } = require("../services/hash");
const { findByUsername } = require("../repositories/account");
const { Router } = require("express");

const { generateToken, verifyToken } = require("../services/token");

const TOKEN_LIFE = 3600;
const SECRET_KEY = process.env.JWT_SECRET_KEY || "onlinechess";

const router = Router();

router.post("/", async (req, res) => {
  const { username, password } = req.body;
  if (!username || !password) {
    res.status(400).send({
      msg: "Invalid username or password",
    });
  } else {
    try {
      const result = await findByUsername(username);
      const account = result.rows[0];
      if (!account) {
        res.status(401).send({
          msg: "Login failed",
        });
      } else {
        const isMatched = await checkPassword(password, account.password);
        if (isMatched) {
          const data = { username };
          const token = await generateToken(data, SECRET_KEY, TOKEN_LIFE);
          res.status(200).send({
            msg: "Login successful",
            token,
          });
        } else {
          res.status(401).send({
            msg: "Login failed",
          });
        }
      }
    } catch (error) {
      console.error(error);
      console.trace();
      res.status(500).send({
        msg: "Not available",
      });
    }
  }
});

module.exports = router;
