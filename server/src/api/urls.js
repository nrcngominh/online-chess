const { Router } = require("express");
const { createGameUrl } = require("../services/online");
const redisClient = require("../services/redis-client");

const WAITING = "WAITING";

const router = Router();

router.get("/:url", async (req, res) => {
  const url = req.params.url;
  try {
    const value = await redisClient.get(url);
    if (value === WAITING) {
      res.status(200).send({
        msg: "Ready",
      });
    } else {
      res.status(404).send({
        msg: "Not found",
      });
    }
  } catch (error) {
    res.status(500).send({
      msg: "Not available",
    });
  }
});

router.post("/", async (req, res) => {
  const url = createGameUrl();
  const waitingSecond = 600;
  const isSet = await redisClient.setex(url, waitingSecond, WAITING);
  if (isSet) {
    res.status(200).send({ url });
  } else {
    res.status(500).send({
      msg: "Error",
    });
  }
});

module.exports = router;
