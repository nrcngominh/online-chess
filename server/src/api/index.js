const { Router } = require("express");

const router = Router();

router.use("/urls", require("./urls"));
router.use("/login", require("./login"));
router.use("/register", require("./register"));
router.use("/auth", require("./auth"));

module.exports = router;
