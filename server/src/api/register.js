const { hashPassword } = require("../services/hash");
const { createOne, findByUsername } = require("../repositories/account");
const { Router } = require("express");

const router = Router();

router.post("/", async (req, res) => {
  // TODO
  const { username, password, confirmPassword } = req.body;
  const hashedPassword = await hashPassword(password);
  const result = await findByUsername(username);
  const isExist = result.rows[0];
  //create account and add to db
  if (!username || !password || !confirmPassword) {
    res.status(400).send({ msg: "Invalid username or password" });
  } else if (password !== confirmPassword) {
    res.status(400).send({ msg: "Repeat password wrong" });
  } else if (isExist) {
    res.status(400).send({ msg: "Username already exists" });
  } else {
    try {
      await createOne(username, hashedPassword);
      res.status(200).send({ msg: "Register successful" });
    } catch (error) {
      console.log(error);
      console.trace();
      res.status(500).send({
        msg: "Server error",
      });
    }
  }
});

module.exports = router;
