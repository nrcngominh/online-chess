const app = require("./app.js")();
const server = require("http").Server(app);
const io = require("socket.io")(server);
const pgClient = require("./services/postgres-client");

const main = async () => {
  try {
    await pgClient.connect();
    console.log("Connected to PostgreSQL");

    io.on("connection", (socket) => {
      console.log("A client connected!");
    });

    server.listen(process.env.PORT || 3000, () => {
      console.log("Server is running on port", server.address().port);
    });
  } catch (error) {
    console.error(error);
    console.trace();
  }
};

main();
